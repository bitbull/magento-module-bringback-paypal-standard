# Bring back PayPal Standard configuration for Magento CE 1.9+

## Facts
- version: 1.0.1
- extension key: Bitbull_BringBackPaypalStandard
- extension on Magento Connect: -
- [extension on Bitbucket](https://bitbucket.org/bitbull/magento-module-bringback-paypal-standard)
- [direct download link](https://bitbucket.org/bitbull/magento-module-bringback-paypal-standard/get/master.zip)
- Composer key: `bitbull/magento-module-bringback-paypal-standard`

## Description
With the release of Magento 1.9, the payment method PayPal Website Payments Standard was deprecated, and thus is no longer available to configure it in the admin panel. This module bring it back.

When installed, the following box become available under PayPal section in System -> Configuration -> Sales -> Payment Methods:

![Screenshot 2015-10-28 01.07.47.png](https://bitbucket.org/repo/bbE9R8/images/997871481-Screenshot%202015-10-28%2001.07.47.png)

## Installation Instructions

1. Install via Composer, modman or by copying all the files from the repository to the appropriate folders
2. Clear the cache
3. That's it :)

## Support

If you have any issues with this extension, please drop a line to gennaro.vietri@bitbull.it

## Contribution

Any contributions are highly appreciated. The best way to contribute code is to open a
[pull request on Bitbucket](https://www.atlassian.com/git/tutorials/making-a-pull-request/).

## Developer

Gennaro Vietri
[@kesonno](https://twitter.com/kesonno)

## Licence

[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

## Copyright

(c) 2015 Bitbull srl